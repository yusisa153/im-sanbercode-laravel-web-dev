<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film', function (Blueprint $table) {
            $table->id('film_id');
            $table->string('judul_film',100);
            $table->text('ringkasan_film');
            $table->integer('tahun_film');
            $table->string('poster_film');
            $table->unsignedBigInteger('genre_id');
            $table->foreign('genre_id')->references('genre_id')->on('genre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film');
    }
};
