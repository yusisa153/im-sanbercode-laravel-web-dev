<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peran', function (Blueprint $table) {
            $table->id('peran_id');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('film_id')->on('film');
            $table->unsignedBigInteger('cast_id');
            $table->foreign('cast_id')->references('cast_id')->on('cast');
            $table->string('nama_peran',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peran');
    }
};
