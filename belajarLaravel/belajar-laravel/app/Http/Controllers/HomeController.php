<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function MasterTemplate()
    {
        // dd('masuk sini');
        return view ('template.masterTemplate');
    }

    public function Table()
    {
        return view ('tabelTugas13.table');
    }

    public function DataTables()
    {
        return view ('tabelTugas13.dataTable');
    }
}