<?php

require_once "animal.php";

class Ape extends Animal
{
    public $name;
    public $legs = 2;
    public $yell = "Auo!";

    public function __construct($nama)
    {
        $this->name = $nama;
    }

    public function CaraBersuara($yaini)
    {
        $this->yell = $yaini;
    }
}
?>