<?php

require_once "animal.php";

class Frog extends Animal
{
    public $name;
    public $jump = "Hop Hop!";

    public function __construct($nama)
    {
        $this->name = $nama;
    }

    public function CaraJalan($lompat)
    {
        $this->jump = $lompat;
    }
}
?>